<?php

if (user_access('forum_razd_create')) {
    if (isset($_POST['create'])) {
        $name = mysql_real_escape_string(trim($_POST['name']));
        $description = mysql_real_escape_string(trim($_POST['description']));
        $number = intval($_POST['number']);
        $type = (isset($_POST['type'])) ? 1 : 0;
        $output = (isset($_POST['output'])) ? 1 : 0;
        $isset_name = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_razdels` WHERE `name` = "'.$name.'" AND `id_forum` = '.$forum->id), 0);
        $isset_number = mysql_result(mysql_query('SELECT COUNT(*) FROM `forum_razdels` WHERE `number` = '.$number.' AND `id_forum` = '.$forum->id), 0);
        if (preg_match("/[^(\w)|(\x7F-\xFF)|(\s)|(\.\-)]/", $_POST['name'], $m)) {
            ?>
            <div class = 'err'>
                В поле &laquo;Название раздела&raquo; присутствуют запрещенные <span style="font-weight: bold; color: red;"><?= $m[0]?></span> символы!.
            </div>
            <?            
        } else        
        if (mb_strlen($name) < $set['forum_new_them_name_min_pod'] || mb_strlen($name) > $set['forum_new_them_name_max_pod']) {
            ?>
            <div class = 'err'>В поле &laquo;Название раздела&raquo; можно использовать от <?= $set['forum_new_them_name_min_pod'] ?> до <?= $set['forum_new_them_name_max_pod'] ?> символов.</div>
            <?
        } elseif ($number < 0 || $number == NULL || $number == 0) {
            ?>
            <div class = 'err'>Введите уровень.</div>
            <?
        } elseif (mb_strlen($description) > $set['forum_new_them_name_max_pod_opis']) {
            ?>
            <div class = 'err'>Слишком длинное описание раздела.</div>
            <?
        } elseif ($isset_number > 0) {
            ?>
            <div class = 'err'>Данный уровень уже использует другой раздел этого подфорума.</div>
            <?
        } elseif ($isset_name > 0) {
            ?>
            <div class = 'err'>Раздел с таким названием уже существует в этом подфоруме.</div>
            <?
        } else {
            admin_log('Форум', 'Разделы', 'Создание раздела "'.$name.'" в подфоруме "'.$forum->name.'".');
            mysql_query('INSERT INTO `forum_razdels` SET `id_forum` = '.$forum->id.', `name` = "'.$name.'", `description` = "'.$description.'", `number` = '.$number.', `type` = '.$type.', `output` = '.$output);
            $_SESSION['msg'] = '<div class = "msg">Раздел успешно создан.</div>';
            header('Location: '.FORUM.'/'.$forum->id.'/');
            exit;
        }
    } elseif (isset($_POST['cancel'])) {
        header('Location: '.FORUM.'/'.$forum->id.'/');
        exit;
    }
    $next_number = mysql_result(mysql_query('SELECT MAX(`number`) FROM `forum_razdels` WHERE `id_forum` = '.$forum->id), 0) + 1;
    ?>
    <div class = 'menu_razd' style = 'text-align: left'>
        <a href = '<?= FORUM ?>'>Форум</a> / <a href = '<?= FORUM.'/'.$forum->id ?>/'><?= output_text($forum->name, 1, 1, 0, 0, 0) ?></a> / Создание раздела
    </div>
    <form action = '<?= FORUM.'/'.$forum->id ?>/create_razdel.html' method = 'post' class="p_m">
        <b>Название раздела (<?= $set['forum_new_them_name_max_pod'] ?> символ(а/ов)):</b><br />
        <input type = 'text' name = 'name' value = '' /><br /><br />
        <b>Описание раздела (<?= $set['forum_new_them_name_max_pod_opis'] ?> символ(а/ов)):</b><br />
        <textarea name = 'description'></textarea><br /><br />
        <b>Позиция:</b> <input type = 'text' name = 'number' value = '<?= $next_number ?>' size = '3' /><br />
        <br /><b>Могут создавать темы только:</b><br />
        <label><input type = 'checkbox' name = 'type' value = '1' /> Администраторы + модераторы</label><br />
        <br /><b>Вывод раздела:</b><br />
        <label><input type = 'checkbox' name = 'output' value = '1' /> Отображать посл. 3 темы вместо описания</label><br /><br />
        <input type = 'submit' name = 'create' value = 'Создать' /> <input type = 'submit' name = 'cancel' value = 'Отменить' />
    </form>
    <?
    include_once '../sys/inc/tfoot.php';
} else {
    header('Location: '.FORUM.'/'.$forum->id.'/');
}
exit;

?>